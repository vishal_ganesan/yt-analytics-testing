import React, { Component } from 'react';
import ChannelDetails from './channelDetails';
import youtube from '../api/youTube';
import VideoList from './videoList';
import PlayList from './playList';
import FeaturedChannels from './featuredChannel';
import { Tabs } from 'antd';
import 'antd/dist/antd.css';
const { TabPane } = Tabs;
export default class base extends Component {
	constructor(props) {
		super(props);

		this.state = {
			searchBarText: '',
			channelUserName: '',
			channelDetails: '',
			videoList: [],
			playList: [],
			videosNextPageToken: '',
			playListNextPageToken: '',
			uploadsId: '',
			videoId: '',
			channelId: '',
			videoId5: [],
			videoStatz: [],
		};
	}
	videoStats = () => {
		youtube
			.get('/videos', {
				params: {
					part: 'statistics,snippet',
					// maxResults: 20,
					key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg',
					id: this.state.videoId.toString(),
				},
			})
			.then(async (res) => {
				console.log(res);
				await this.setState({ videoStatz: res.data.items });
			})
			.catch((error) => {
				console.log(error);
			});
	};
	loadMoreVideos = () => {
		youtube
			.get('/videos', {
				params: {
					part: 'statistics,snippet',
					// maxResults: 20,
					key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg',
					id: this.state.videoId5.toString(),
				},
			})

			.then(async (res) => {
				await this.setState({ videoId5: [] });
				this.setState({
					videoStatz: [...this.state.videoStatz, ...res.data.items],
				});
				// await this.props.nextIdsNull()
			})
			.catch((error) => {
				console.log(error);
			});
	};
	videoList = async (uploadsId) => {
		// console.log('upId:', uploadsId);
		await youtube
			.get('/playlistItems', {
				params: {
					part: 'snippet,contentDetails',
					maxResults: 50,
					key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg ',
					playlistId: uploadsId,
				},
			})
			.then((response) => {
				// console.log('video', response);
				const videoIds = response.data.items.map(
					(item) => item.snippet.resourceId.videoId
				);
				this.setState({
					videoList: response.data.items,
					videosNextPageToken: response.data.nextPageToken,
					videoId: videoIds,
				});
			})
			.catch((error) => {
				console.log(error);
			});
		await this.videoStats();
	};
	playListContent = (channelId) => {
		// console.log('channelId', channelId);
		youtube
			.get('/playlists', {
				params: {
					part: 'snippet,contentDetails',
					// maxResults: 5,
					key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg ',
					channelId: channelId,
				},
			})
			.then((response) => {
				// console.log('playlist', response);
				this.setState({
					playList: response.data.items,
					playListNextPageToken: response.data.nextPageToken,
				});
			})
			.catch((error) => {
				console.log(error);
			});
	};

	channelDetailsFromFeaturedChannel = (
		channelDetails,
		uploadsId,
		channelId
	) => {
		this.setState({ channelDetails });
		this.videoList(uploadsId);
		this.playListContent(channelId);
	};

	render() {
		console.log(this.state.videoStatz);
		return (
			<div className='container mt-5'>
				<input
					type='text'
					id='fname'
					name='fname'
					onChange={(e) => {
						this.setState({ searchBarText: e.target.value });
					}}
				></input>
				<button
					type='button'
					onClick={async () => {
						await this.setState({ channelUserName: this.state.searchBarText });
						youtube
							.get('/channels', {
								params: {
									part: 'statistics,contentDetails,snippet,brandingSettings',
									// maxResults: 20,
									key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg ',
									forUsername: this.state.channelUserName,
								},
							})
							.then((response) => {
								// console.log(response.data.items[0]);
								this.setState({
									channelDetails: response.data.items[0],
									uploadsId:
										response.data.items[0].contentDetails.relatedPlaylists
											.uploads,
									channelId: response.data.items[0].id,
								});
								this.videoList(
									response.data.items[0].contentDetails.relatedPlaylists.uploads
								);
								this.playListContent(response.data.items[0].id);
							})
							.catch((error) => {
								console.log(error);
							});
					}}
				>
					search
				</button>
				{!this.state.channelDetails ? (
					<div>Please enter channel user name.</div>
				) : (
					<div className='mt-5'>
						<ChannelDetails channelDetails={this.state.channelDetails} />
					</div>
				)}

				{this.state.videoList.length ? (
					<Tabs defaultActiveKey='1'>
						<TabPane tab='Videos' key='1'>
							<div>
								<VideoList
									videoList={this.state.videoList}
									videoIdsList={this.state.videoId}
									videoStats={this.state.videoStatz}
									nextIds={this.state.videoId5}
								/>
								<button
									type='button'
									className='btn btn-primary'
									onClick={async () => {
										await youtube
											.get('/playlistItems', {
												params: {
													part: 'snippet,contentDetails',
													maxResults: 5,
													key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg ',
													playlistId: this.state.uploadsId,
													pageToken: this.state.videosNextPageToken,
												},
											})
											.then(async (response) => {
												const videoIds = response.data.items.map(
													(item) => item.snippet.resourceId.videoId
												);
												// console.log(...this.state.videoId, ...videoIds);
												await this.setState({
													videoId5: videoIds,
													videosNextPageToken: response.data.nextPageToken,
												});
											})
											.catch((error) => {
												console.log(error);
											});
										// console.log(this.state.videoList);
										await this.loadMoreVideos();
									}}
								>
									Load More
								</button>
							</div>
						</TabPane>
						<TabPane tab='Playlist' key='2'>
							{this.state.playList.length ? (
								<div>
									<PlayList playList={this.state.playList} />
									<button
										type='button'
										class='btn btn-primary'
										onClick={() => {
											youtube
												.get('/playlists', {
													params: {
														part: 'snippet,contentDetails',
														maxResults: 5,
														key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg ',
														channelId: this.state.channelId,
														pageToken: this.state.playListNextPageToken,
													},
												})
												.then(async (response) => {
													console.log(
														...this.state.playList,
														...response.data.items
													);
													await this.setState({
														videoList: [
															...this.state.playList,
															...response.data.items,
														],
														videosNextPageToken: response.data.nextPageToken,
													});
												})
												.catch((error) => {
													console.log(error);
												});
											// console.log(this.state.videoList);
										}}
									>
										Load More
									</button>
								</div>
							) : (
								<h1>no playlist found</h1>
							)}
						</TabPane>
						{this.state.channelDetails.brandingSettings.channel
							.featuredChannelsUrls.length ? (
							<TabPane tab='Featured Channels' key='3'>
								<FeaturedChannels
									featuredChannelsId={
										this.state.channelDetails.brandingSettings.channel
											.featuredChannelsUrls
									}
									channelDetails={this.channelDetailsFromFeaturedChannel}
								/>
							</TabPane>
						) : null}
					</Tabs>
				) : null}
			</div>
		);
	}
}
