import React, { Component } from 'react';
import youtube from '../api/youTube';

export default class featuredChannel extends Component {
	state = {
		featuredChannel: [],
	};
	componentDidMount() {
		youtube
			.get('/channels', {
				params: {
					part: 'statistics,contentDetails,snippet,brandingSettings',
					// maxResults: 20,
					key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg ',
					id: this.props.featuredChannelsId.toString(),
				},
			})
			.then((response) => {
				// console.log(response);
				this.setState({ featuredChannel: response.data.items });
			})
			.catch((error) => {
				console.log(error);
			});
	}
	numberWithCommas = (x) => {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	};
	render() {
		return (
			<div className='row d-flex'>
				{this.state.featuredChannel.map((channel) => (
					<div
						class='card col-3 m-1 p-1'
						onClick={() => {
							console.log(channel);
							this.props.channelDetails(
								channel,
								channel.contentDetails.relatedPlaylists.uploads,
								channel.id
							);
						}}
					>
						<img
							src={channel.snippet.thumbnails.high.url}
							className='card-img-top'
							alt={channel.snippet.title}
						/>
						<div className='card-body'>
							<h5 className='card-title'>{channel.snippet.title}</h5>
							<p>
								<b>No. of videos:</b>{' '}
								{this.numberWithCommas(channel.statistics.videoCount)}
							</p>
							<p>
								<b>No. of views:</b>{' '}
								{this.numberWithCommas(channel.statistics.viewCount)}
							</p>
							<p>
								<b>No. of subscriber:</b>{' '}
								{this.numberWithCommas(channel.statistics.subscriberCount)}
							</p>
						</div>
					</div>
				))}
			</div>
		);
	}
}
