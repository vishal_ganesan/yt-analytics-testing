import React, { Component } from 'react';
import youtube from '../api/youTube';
import { Table } from 'antd';
export default class videoList extends Component {
	state = {
		videoStats: [],
		// isLoading: true,
		// videoId: this.props.videoIdsList,
	};

	numberWithCommas = (val) => {
		var x = val.toString();
		var lastThree = x.substring(x.length - 3);
		var otherNumbers = x.substring(0, x.length - 3);
		if (otherNumbers !== '') lastThree = ',' + lastThree;
		var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree;
		return res;
	};
	// componentDidMount() {
	// 	if (!this.state.videoStats.length) {
	// 		youtube
	// 			.get('/videos', {
	// 				params: {
	// 					part: 'statistics,snippet',
	// 					// maxResults: 20,
	// 					key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg',
	// 					id: this.props.videoIdsList.toString(),
	// 				},
	// 			})
	// 			.then(async (res) => {
	// 				await this.setState({ videoStats: res.data.items, isLoading: false });
	// 			})
	// 			.catch((error) => {
	// 				console.log(error);
	// 			});
	// 	}
	// }
	// shouldComponent=()=> {
	// 	// console.log(this.state.videoStats, this.props.videoIdsList);
	// 	if (this.props.nextIds.length) {
	// 		youtube
	// 			.get('/videos', {
	// 				params: {
	// 					part: 'statistics,snippet',
	// 					// maxResults: 20,
	// 					key: 'AIzaSyDOuyyCjSbfd0zMV9oLTAlmJ-cBKsDsTVg',
	// 					id: this.props.nextIds.toString(),
	// 				},
	// 			})

	// 			.then(async (res) => {
	// 				await this.props.nextIdsNull();
	// 				this.setState({
	// 					videoStats: [...this.state.videoStats, ...res.data.items],
	// 				});
	// 				// await this.props.nextIdsNull()
	// 			})
	// 			.catch((error) => {
	// 				console.log(error);
	// 			});
	// 		return true;
	// 	}
	// }
	render() {
		// console.log(this.props.nextIds);
		const VideoListTableData = this.props.videoStats.map((video, index) => {
			// console.log(video);
			return {
				sno: index + 1,
				title: video.snippet.title,
				publisheAt: new Date(video.snippet.publishedAt).toDateString(),
				viewCount: video.statistics.viewCount
					? this.numberWithCommas(video.statistics.viewCount, index)
					: 0,
				likes: video.statistics.likeCount
					? this.numberWithCommas(video.statistics.likeCount, index)
					: 0,
				dislikes: video.statistics.dislikeCount
					? this.numberWithCommas(video.statistics.dislikeCount, index)
					: 0,
				comments: video.statistics.commentCount
					? this.numberWithCommas(video.statistics.commentCount, index)
					: 0,
			};
		});
		// console.log(VideoList1);
		const videoListTableColumnNames = [
			{ title: 'Sno', dataIndex: 'sno', key: 'sno' },
			{ title: 'Title', dataIndex: 'title', key: 'title' },
			{ title: 'Publishe At', dataIndex: 'publisheAt', key: 'publisheAt' },
			{ title: 'View Count', dataIndex: 'viewCount', key: 'viewCount' },
			{ title: 'Likes', dataIndex: 'likes', key: 'likes' },
			{ title: 'Dislikes', dataIndex: 'dislikes', key: 'dislikes' },
			{ title: 'Comments', dataIndex: 'comments', key: 'comments' },
		];
		// }

		return !this.state.isLoading ? (
			<>
				<Table
					dataSource={VideoListTableData}
					columns={videoListTableColumnNames}
					pagination={false}
				/>
				{/* {this.shouldComponent()} */}
			</>
		) : (
			<h1>loading</h1>
		);
	}
}
