import React, { Component } from 'react';

export default class channelDetails extends Component {
	state = {
		channelDetails: {},
		isLoading: false,
	};

	numberWithCommas = (val) => {
		// console.log(x);
    var x = val.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers !== '') lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree;
    return res;
	};
	render() {
		// console.log(this.props);

		const channelDetails = this.props.channelDetails;
		return !this.state.isLoading ? (
			<div>
				<p>
					<b>Title:</b> {channelDetails.snippet.title}
				</p>
				<p>
					<b>Description:</b> <span>{channelDetails.snippet.description}</span>{' '}
				</p>
				<div className='d-flex justify-content-between'>
					<p>
						<b>Created At:</b>{' '}
						{new Date(channelDetails.snippet.publishedAt).toDateString()}
					</p>
					<p>
						<b>Views:</b>{' '}
						{channelDetails.statistics.viewCount
							? this.numberWithCommas(channelDetails.statistics.viewCount)
							: 0}
					</p>
					<p>
						<b>Subscribers:</b>{' '}
						{channelDetails.statistics.subscriberCount
							? this.numberWithCommas(channelDetails.statistics.subscriberCount)
							: 0}
					</p>
					<p>
						<b>Videos:</b>{' '}
						{channelDetails.statistics.videoCount
							? this.numberWithCommas(channelDetails.statistics.videoCount)
							: 0}
					</p>
				</div>
			</div>
		) : (
			<div>Loadin...</div>
		);
	}
}
