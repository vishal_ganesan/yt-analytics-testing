import React, { Component } from 'react';

export default class playList extends Component {
  numberWithCommas = (x) => {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	};
	render() {
		return (
			<div className='row d-flex'>
				{this.props.playList.map((playListItem) => {
					return (
						<div class='card col-3 m-1 p-1' style={{ width: '18rem;' }}>
							<img
								src={playListItem.snippet.thumbnails.high.url}
								class='card-img-top'
								alt={playListItem.snippet.title}
							/>
							<div class='card-body'>
								<h5 class='card-title'>{playListItem.snippet.title}</h5>
								<p class='card-text'>{playListItem.snippet.description}</p>
								<p>
									<b>No. of videos:</b> {playListItem.contentDetails.itemCount}
								</p>
							</div>
						</div>
					);
				})}
			</div>
		);
	}
}
